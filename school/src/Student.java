public class Student extends Person{


        private double examGrade;

        public Student(Long cnp, String firstName, String lastName, int age, double examGrade) {
            super(cnp, firstName, lastName, age);
            this.examGrade = examGrade;
        }


        public double getExamGrade() {
            return examGrade;
        }

        public void setExamGrade(double examGrade) {
            this.examGrade = examGrade;
        }

        @Override
        public String toString(){
        return "Salut! Eu sunt un student "+ super.toString() + " media mea pe anul trecut a fost: "+examGrade;
    }
}