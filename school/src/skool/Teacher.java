package skool;

public class Teacher extends Person {

    private String subject;

    public Teacher(Long cnp, String firstName, String lastName, int age, String subject) {
        // super apelează constructorul SUPERclasei Person
        super(cnp, firstName, lastName, age);
        this.subject = subject;
    }

    public Teacher(Long cnp, String subject) {
        // super apelează constructorul SUPERclasei Person
        super(cnp);
        this.subject = subject;
    }

    @Override
    public String toString(){
        return "Salut! Eu sunt un profesor";
    }
}
