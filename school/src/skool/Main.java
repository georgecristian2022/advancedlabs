package skool;

public class Main {


    /**
     * Student
     * Teacher
     * Employee - Director, Administrator, etc
     */
    public static void main(String[] args) {
        // write your code here

        Teacher t1 = new Teacher(
                1234567890567L,
                "Maria",
                "Popescu",
                46,
                "Engleză"
        );

        System.out.println(t1.getFirstName());

        System.out.println(t1.toString());

        Person p1 = new Person(1111L,"george","cristian",19);

        Student s1 = new Student(999L,"gigi","dobrica", 17,9);

        Employee e1 = new Employee(444L, "Director");

        System.out.println(p1.toString());
        System.out.println(s1.toString());
        System.out.println(e1.toString());
    }
}
