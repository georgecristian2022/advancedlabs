package skool;

public class Employee extends Person {

        private String occupation;

        public Employee(Long cnp, String firstName, String lastName, int age, String occupation) {
            // super apelează constructorul SUPERclasei Person
            super(cnp, firstName, lastName, age);
            this.occupation = occupation;
        }

        public Employee(Long cnp, String occupation) {
            // super apelează constructorul SUPERclasei Person
            super(cnp);
            this.occupation = occupation;
        }

        @Override
        public String toString(){
            return "Salut! Eu sunt un un angajat cu functia de: "+occupation;
        }
    }