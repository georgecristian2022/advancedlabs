package shape;
public class Triangle extends Shape {
    public int height;

    public Triangle(int height, int base){
        super(base);
        this.height=height;
    }

    @Override
    public Integer getArea(){
        return (height * super.base)/2;
    }
}
